"# SEO" 
# SEO
<h3> On Page SEO Helpful Tools and Website </h3>

<b>Domain Blacklist Check  </b> <br/>
1. [Is My Website Penalized](https://ismywebsitepenalized.com/) <br/>
2. [Google Transparency report](https://transparencyreport.google.com/) <br/>
  
<b>Domain and Page Authority </b><br/>
1. [Moz Bar](https://chrome.google.com/webstore/detail/mozbar/eakacpaijcpapndcfffdgphdiccmpknp)<br/>

<b> Browser Compatability Test </b> <br/>
1. [Browser Shots](http://browsershots.org/) <br/>
2. [User Agent Switcher](https://chrome.google.com/webstore/detail/user-agent-switcher-for-c/djflhoibgkdhkhhcedjiklpkjnoahfmg)-Chrome Plugin <br/>
 
  
  <b> Keyword Research </b><br/>
  1. [Answer the public](https://answerthepublic.com) <br/>
  2. [Uber suggest](https://neilpatel.com/ubersuggest/) <br/> 
  3. [Soolve](http://www.soovle.com/) <br/>
  
<b>Code Generator</b><br/>
  1. [Webcode Tools](https://webcode.tools/) - For Meta tags,OG property, twitter cards, structured data etc code generation <br/>

 <b> LSI Keyword Generator</b><br/>  
  1. [LSI Graph](https://lsigraph.com/) <br/>

<b> Structured Data Testing Tool </b></br>
 1. [Google Structured Data Testing](https://search.google.com/structured-data/testing-tool)<br/>

  
  <b> Page Speed </b><br/>
  1. [Page Speed Insights](https://developers.google.com/speed/pagespeed/insights/) <br/>
  2. [Webpage Test](http://www.webpagetest.org/video/)- Create videos comparing site speed against competitors <br/>
  

  
  [Tools by Google](https://developers.google.com/search/tools/) <br/>
  
  
  <h3> Off Page and Marketing Tools and Websites</h3>
 
 <b> Google My Business</b> <br/>
1. [Google Business Review Direct Link Generator](https://supple.com.au/tools/google-review-link-generator/)<br/>

  
<b> Youtube</b><br/>
1. [Keywords and Analytics](https://chrome.google.com/webstore/detail/vidlog/pmnenkkakioojceckmokdmkdjmkfiafo)- Chrome plugin

